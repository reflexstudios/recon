<?php return array (
  'aryehraber/statamic-captcha' => 
  array (
    'id' => 'aryehraber/statamic-captcha',
    'slug' => 'captcha',
    'editions' => 
    array (
    ),
    'marketplaceId' => 277,
    'marketplaceSlug' => 'captcha',
    'marketplaceUrl' => 'https://statamic.com/addons/aryeh-raber/captcha',
    'marketplaceSellerSlug' => 'aryeh-raber',
    'latestVersion' => '1.8.0',
    'version' => '1.8.0',
    'namespace' => 'AryehRaber\\Captcha',
    'autoload' => 'src',
    'provider' => 'AryehRaber\\Captcha\\CaptchaServiceProvider',
    'name' => 'Captcha',
    'url' => NULL,
    'description' => 'Protect your Statamic forms using a Captcha service',
    'developer' => 'Aryeh Raber',
    'developerUrl' => 'https://aryeh.dev',
    'email' => NULL,
  ),
);