---
id: 2284cd72-10f7-4f2f-92a7-daf5146e14c2
blueprint: pages
title: Products
author: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1653487694
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - placeholders/stones.jpeg
        title: Products
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We pride ourselves in producing some of the highest quality, consistent recycled products in the UK. The products are used locally in the construction and agricultural industries. All products are analysed on a regular basis to ensure they conform with end of waste specifications.'
        button_link: false
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - placeholders/topsoil.jpeg
        title: Topsoil
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Screened topsoil is produced on site to BS3882. Soil is screened to 10mm and is suitable to lawn dressing, landscaping and horticultural applications.\_"
        button_link: 'entry::f31e0525-4e56-4328-88a1-2b0319019506'
        button_text: 'More Info'
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: true
        image:
          - placeholders/handsStones2.jpeg
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Our Pea Gravel is produced to the WRAP Quality Protocol for the Production of Aggregates from Inert Waste Standard. Pea Gravel is 10mm on average and is great for pipe bedding, shoring, concrete production and a variety of applications.'
        title: 'Pea Gravel'
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - placeholders/sand.jpeg
        title: Sand
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Sand is produced to the WRAP Quality Protocol for the Production of Aggregates from Inert Waste Standard. The product is great for a variety of applications including concrete production and utility service cover.'
  -
    type: paragraph
---
