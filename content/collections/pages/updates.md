---
id: f08c7c40-0849-48a9-859f-5dc4ff61adac
blueprint: pages
template: news/index
title: Updates
author: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_by: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_at: 1643375035
general_content:
  -
    type: set
    attrs:
      values:
        type: hero_title_only
        title: 'Stay up to date on the latest news and events from Recon'
  -
    type: paragraph
---
