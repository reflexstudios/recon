---
id: f31e0525-4e56-4328-88a1-2b0319019506
blueprint: pages
title: Topsoil
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - placeholders/topsoil.jpeg
        title: Topsoil
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Screened topsoil is produced on site to BS3882. Soil is screened to 10mm and is suitable to lawn dressing, landscaping and horticultural applications.\_"
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image: placeholders/machine.jpeg
        text:
          -
            type: set
            attrs:
              values:
                type: table
                table:
                  -
                    cells:
                      - Collected
                      - 'per tonne'
                  -
                    cells:
                      - 'Car Trailer small 1 tonne'
                      - '£40 + vat'
                  -
                    cells:
                      - 'Car Trailer medium 1-2 tonnes'
                      - '£50 + vat'
                  -
                    cells:
                      - 'More than 2 tonnes: '
                      - '£20/tonne + Vat'
                first_row_is_header: true
          -
            type: heading
            attrs:
              level: 4
            content:
              -
                type: text
                text: "Delivered: \_"
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Give us a call and we will quote you for delivery to your specified area!'
        title: Prices
  -
    type: set
    attrs:
      values:
        type: product_enquiry
        flip_image_and_text: true
        small_title: 'Screened Topsoil'
        title: 'Enquire Now'
        options_prices:
          -
            option_title: 'Car Trailer small 1 tonne'
            price: '£40 + vat'
          -
            option_title: 'Car Trailer medium 1-2 tonnes'
            price: '£50 + vat'
          -
            option_title: 'More than 2 tonnes: '
            price: '£20/tonne + Vat'
        image: placeholders/soilpile.jpeg
  -
    type: paragraph
parent: 2284cd72-10f7-4f2f-92a7-daf5146e14c2
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1653497438
page_thumbnail: placeholders/topsoil.jpeg
---
