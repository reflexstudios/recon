---
id: bc90a03b-ea41-473e-b780-fa22f1705272
blueprint: pages
title: Topsoil
updated_by: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_at: 1644341651
short_page_description: 'State of the art soil processing plant allows us to manufacture screened topsoil to BS3882.'
page_thumbnail: placeholders/machine.jpeg
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - placeholders/machine.jpeg
        title: Topsoil
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'ReCon is a long-established provider of topsoil to both commercial and private projects throughout Ireland. Our soil is of the highest quality and conforms with BS3822.'
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - placeholders/topsoil.jpeg
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Topsoil can be collected on site or delivered to site using our modern fleet of bulk tipper lorries.\_"
  -
    type: paragraph
---
