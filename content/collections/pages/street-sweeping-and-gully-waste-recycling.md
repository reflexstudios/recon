---
id: dd6f0849-9a02-43ca-9ceb-a511067cae9a
blueprint: pages
title: 'Street Sweeping and Gully Waste'
updated_by: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_at: 1644341278
page_thumbnail: placeholders/hangs.png
short_page_description: 'Our street sweeping processing systems offers recycling rates of +99%'
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - placeholders/redmachine.jpeg
        title: 'Street Sweeping and Gully Waste'
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "ReCon has been a market leader in the recycling of street sweepings since in investing in washing and screening technology over 10 years ago. We continue to develop our processes to create innovative new products for the local construction industry.\_"
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - placeholders/stones.jpeg
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Our custom built wash plant allows us to divert 100% of material from landfill with a 99% \_recycling rate."
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: true
        image:
          - placeholders/screen.png
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "We use state of the art screeners and optical sorting technology to size and clean material in order to meet end of waste product specifications.\_"
  -
    type: paragraph
---
