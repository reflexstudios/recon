---
id: 34e5413a-44a3-4965-9eb9-05ccdca6a5e4
blueprint: pages
title: 'Construction and Demolition Waster'
parent: 05202b7f-1fce-47f8-8ec8-6264fa40c0aa
page_thumbnail: placeholders/jcb2.jpeg
short_page_description: 'C&D waste is processing on site to produce WRAP approved recycled aggregates.'
updated_by: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_at: 1644341545
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - placeholders/buildingwaste.jpeg
        title: 'Construction and Demolition Waster'
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Construction and demolition waste is processed on site to produce high quality recycled aggregates that are used in local construction projects.\_"
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - placeholders/hands2.jpeg
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Hardcore is crushed and screened to produce various grades of aggregate that are analysed against the WRAP Quality Protocol standards.\_"
  -
    type: paragraph
---
