---
id: c759c7b8-3b63-4bab-8e7d-7ed42da6efe6
blueprint: jobs
template: jobs/index
title: Careers
author: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1653495620
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - ReconLorry.jpg
        title: Careers
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'ReCon are always on the lookout for individuals who will bring something new to the team. If you would like to build a career at ReCon please forward your CV to info@reconwastemanagement.com. We pride ourselves in having a close knit team of people who enjoy doing what they do!'
          -
            type: paragraph
            content:
              -
                type: text
                marks:
                  -
                    type: bold
                text: 'We are interested in hearing from:'
          -
            type: bullet_list
            content:
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Waste Professionals'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Class 1 and Class 2 Lorry Drivers'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Machine operators'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: Welder/Fabricators
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: 'Admin and Accounts staff'
              -
                type: list_item
                content:
                  -
                    type: paragraph
                    content:
                      -
                        type: text
                        text: "General operatives.\_"
        image: placeholders/menAndJcb.jpeg
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: jobs
  -
    type: set
    attrs:
      values:
        type: awards
  -
    type: paragraph
---
