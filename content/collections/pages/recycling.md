---
id: 8c518503-3430-4ea1-8444-9e0074c4a61c
blueprint: pages
title: Recycling
updated_by: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_at: 1644340800
short_page_description: 'ReCon specialise in providing solutions for difficult-to-treat waste streams.'
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - placeholders/machines2.jpeg
        title: Recycling
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Since 2011 ReCon Waste Management has grown from strength to strength and has been recognised as an industry leader in the recycling of street sweeping waste and non-hazardous materials.\_"
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - placeholders/stones.jpeg
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "We strive to produce high quality recyclates from waste materials that would have traditionally been destined for landfill.\_"
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: true
        image:
          - placeholders/laptopman2.jpeg
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Our experienced team of waste managers can offer support and advice on a wide range of waste streams and recycling solutions.\_"
  -
    type: paragraph
  -
    type: set
    attrs:
      values:
        type: 2_col_cta
        left_title: Products
        left_text: 'See what these waste materials become after we’ve recycled and treated them at our purpose built facility.'
        left_button_text: 'Find out more'
        left_button_link: 'entry::2284cd72-10f7-4f2f-92a7-daf5146e14c2'
        right_title: Enquire
        right_text: 'Would you like to know more about our recycling services? Get in touch to see how we can help you.'
        right_button_text: 'Get in touch'
        right_button_link: 'entry::e738e012-7ead-42a4-b9eb-b3defce024cf'
  -
    type: set
    attrs:
      values:
        type: awards
  -
    type: paragraph
page_thumbnail: placeholders/earthmover.jpeg
---
