---
id: f0ddc8f4-3a2d-4791-ac35-c59bace18b22
blueprint: pages
title: 'Waste Transfer'
parent: 05202b7f-1fce-47f8-8ec8-6264fa40c0aa
page_thumbnail: placeholders/truck.jpeg
short_page_description: 'We operate a modern fleet of waste collection vehicles including tippers and grab lorries.'
updated_by: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_at: 1644341866
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - placeholders/truck2.jpeg
        title: 'Waste Haulage'
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "ReCon operate a modern fleet of waste collection vehicles and trailers which have been custom built for handling various waste streams.\_"
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - placeholders/truck3.jpeg
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Our transport managers have the experience and knowledge to provide an unrivalled collection service that allows us to lift waste from anywhere in Ireland.\_"
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: true
        image:
          - Screenshot-2022-02-08-at-17.37.11.png
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Our custom designed grab lorries allow us to service multiple skips in the same run. This cuts down our carbon footprint and allows us to pass on savings to the customer.\_"
  -
    type: paragraph
---
