---
id: 05202b7f-1fce-47f8-8ec8-6264fa40c0aa
blueprint: pages
title: Services
author: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1651678547
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - digger.png
        title: 'Green and efficient service for local business sectors'
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "ReCon work closely with local authorities, government departments and local businesses to provide industry leading recycling solutions with the circular economy at their core.\_"
  -
    type: set
    attrs:
      values:
        type: links_list
        links_list:
          -
            image: placeholders/recyclingthing.png
            title: Recycling
            subtext: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
            link: 'entry::8c518503-3430-4ea1-8444-9e0074c4a61c'
            type: new_link
            enabled: true
        entries:
          - 8c518503-3430-4ea1-8444-9e0074c4a61c
          - dd6f0849-9a02-43ca-9ceb-a511067cae9a
          - 6f8b9e0b-9879-498d-941f-0bae68ccfdba
          - 34e5413a-44a3-4965-9eb9-05ccdca6a5e4
          - bc90a03b-ea41-473e-b780-fa22f1705272
          - f0ddc8f4-3a2d-4791-ac35-c59bace18b22
  -
    type: set
    attrs:
      values:
        type: 2_col_cta
        left_title: Products
        left_text: 'See what these waste materials become after we’ve recycled and treated them at our purpose built facility.'
        left_button_text: 'Find Out More'
        left_button_link: 'entry::d64abb9c-baf9-4dd1-8082-cc072f563489'
        right_title: Enquire
        right_text: 'Would you like to know more about any of these services? Get in touch to see how we can help you.'
        right_button_text: 'Get in touch'
        right_button_link: 'entry::e738e012-7ead-42a4-b9eb-b3defce024cf'
  -
    type: set
    attrs:
      values:
        type: awards
  -
    type: paragraph
---
