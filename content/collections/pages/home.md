---
id: home
blueprint: pages
title: Home
template: default
general_content:
  -
    type: set
    attrs:
      values:
        type: large_hero_banner
        background_image:
          - placeholders/hangs.png
        title: 'Committed to a sustainable future.'
        subtext:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'An award winning Waste Management company in Portadown, Northern Ireland - we aim to ease the burden on local councils and business as they look for innovative ways to achieve recycling targets.'
        button_1_text: 'Learn More'
        button_1_link: 'entry::d64abb9c-baf9-4dd1-8082-cc072f563489'
        button_2_text: 'Our Products'
        button_2_link: 'entry::2284cd72-10f7-4f2f-92a7-daf5146e14c2'
  -
    type: set
    attrs:
      values:
        type: 2_col_lists
        col_1_title: Services
        col_1_subtext: 'We provide cost effective service to local business sectors'
        col_1_list:
          -
            title: Recycling
          -
            title: 'Gully Waste Recycling'
          -
            title: 'Sweeper Waste Recycling'
          -
            title: 'Waste Transfer'
          -
            title: 'Soil Production'
        col_1_button_text: 'Learn More'
        col_2_title: Products
        col_2_subtext: 'We process and recycle waste to produce many useful materials'
        col_2_list:
          -
            title: Sand
          -
            title: Topsoil
          -
            title: 'Pea Gravel'
          -
            title: Concrete
          -
            title: 'Drainage Stone'
        col_2_button_text: 'Learn More'
        col_1_button_link: 'entry::05202b7f-1fce-47f8-8ec8-6264fa40c0aa'
        col_2_button_link: 'entry::2284cd72-10f7-4f2f-92a7-daf5146e14c2'
        col_1_entries:
          - 8c518503-3430-4ea1-8444-9e0074c4a61c
          - dd6f0849-9a02-43ca-9ceb-a511067cae9a
          - 6f8b9e0b-9879-498d-941f-0bae68ccfdba
          - 34e5413a-44a3-4965-9eb9-05ccdca6a5e4
          - bc90a03b-ea41-473e-b780-fa22f1705272
          - f0ddc8f4-3a2d-4791-ac35-c59bace18b22
        col_2_entries:
          - f31e0525-4e56-4328-88a1-2b0319019506
  -
    type: set
    attrs:
      values:
        type: news_carousel
  -
    type: set
    attrs:
      values:
        type: awards
  -
    type: paragraph
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1653479492
---
Welcome to your new Statamic website.
