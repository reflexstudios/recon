---
id: d64abb9c-baf9-4dd1-8082-cc072f563489
blueprint: pages
template: default
title: About
author: 73141162-f2e9-4d02-b2fd-a15c8199bdad
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1653487529
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        title: 'Our Story'
        background_image:
          - Group-26.png
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - Frame-26.jpg
        title: 'Over 10 years experience'
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "ReCon Waste Management was established by Daniel Connolly in 2011. Since then, the business and grown from strength to strength and now employs more than 20 people. Services are provided to a number of local authority, commercial and private customers throughout Ireland. The company specialises in finding recycling and recovery solutions for waste streams that have traditionally been sent to landfill.\_"
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: true
        image:
          - about2.jpg
        title: 'Award Winning'
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The company has been recognised by numerous trade bodies for the forward thinking processes we have developed over the years.'
              -
                type: hard_break
              -
                type: hard_break
              -
                type: image
                attrs:
                  src: 'asset::assets::enviro.png'
                  alt: null
              -
                type: image
                attrs:
                  src: 'asset::assets::award4.png'
                  alt: null
              -
                type: image
                attrs:
                  src: 'asset::assets::wishni.png'
                  alt: null
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - placeholders/soil2.jpeg
        title: 'Circular Economy'
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'The circular economy is at the core of everything we do. All of our recycled products are used in local construction products helping reduce the carbon footprint and increase the sustainability of the local economy.'
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: true
        image:
          - placeholders/menAndJcb.jpeg
        title: Team
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Our experienced team have a wealth of knowledge to provide advice on the processing, transport and recycling/recovery of a broad range of waste materials.\_"
  -
    type: set
    attrs:
      values:
        type: 2_col_cta
        left_title: Products
        left_text: 'See what products we create from waste materials after we’ve recycled and treated them at our purpose built facility.'
        left_button_text: 'Find out more'
        left_button_link: 'entry::c759c7b8-3b63-4bab-8e7d-7ed42da6efe6'
        right_title: Services
        right_text: 'We provide industry-leading recycling solutions with the circular economy at their core. '
        right_button_text: 'Find out more'
        right_button_link: 'entry::05202b7f-1fce-47f8-8ec8-6264fa40c0aa'
  -
    type: set
    attrs:
      values:
        type: awards
  -
    type: paragraph
---
