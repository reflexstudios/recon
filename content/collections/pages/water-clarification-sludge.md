---
id: 6f8b9e0b-9879-498d-941f-0bae68ccfdba
blueprint: pages
title: 'Water Clarification Sludge'
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1651586947
short_page_description: 'ReCon has developed an end of waste solution to recycle 100% of this material.'
page_thumbnail: placeholders/dumping.jpeg
general_content:
  -
    type: set
    attrs:
      values:
        type: small_hero_banner
        background_image:
          - placeholders/soilpile.jpeg
        title: 'Water Clarification Sludge'
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'We have pioneered an advanced recycling process that allows 100% of water clarification sludge to be recycled. Sludge is collected from a number of water treatment plants throughout Ireland.'
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: false
        image:
          - placeholders/dumping.jpeg
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: 'Sludge is processed on site to produce recycled soil for the local agriculture industry.'
        title: 'Water Clarification Sludge'
  -
    type: set
    attrs:
      values:
        type: text_and_image
        flip_image_and_text: true
        image:
          - placeholders/siv.jpeg
        text:
          -
            type: paragraph
            content:
              -
                type: text
                text: "Material is analysed both in house and external labs to ensure we are conforming to product specifications.\_"
        title: 'Material Analysis'
  -
    type: paragraph
---
