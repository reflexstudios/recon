---
id: 5daddd91-1af8-4cb7-b9f7-573db12d694a
blueprint: news
title: 'Recon Waste Management Team of the Year'
text:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'The team at Recon Waste Management, led by forward thinking Managing Director Daniel Connolly, has been recognised at the recent annual Plant & Civil Engineer Awards for their successes in the waste industry.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'They were presented with the prestigious Waste Management Team of the Year award, sponsored by Stewart Commercials, during a glittering ceremony, compered by television and radio sports presenter Adrian Logan, at the Crowne Plaza Hotel in South Belfast.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Organised by the Hillsborough based publishers of Plant & Civil Engineer, 4SM (NI) Ltd, it was attended by hundreds of guests and leading figures from the plant, construction and quarry industries. With 17 separate categories, the event, making a welcome return after a two year absence because of the Covid-19 pandemic, is widely regarded as Ireland’s premier platform of recognition for those operating across all sectors of the industry.'
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1670425215
image: img.png
---
