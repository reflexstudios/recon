---
id: 9cefd9c1-5274-489f-81e0-fbaa6a3582d1
blueprint: news
title: 'Minister O’Neill Visits Innovative Waste Management Project'
text:
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Daniel Connolly MD commented “a visit such as this gives a great boost to smaller companies like ourselves.” The Minister was very impressed with our process and said it was great to see farm diversification and innovation at this level in a rural community.'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::2374083641.webp'
          alt: null
      -
        type: text
        text: 'Daniel Connolly, Managing Director of ReCon, tells Minister O’Neill how RDP funding has helped the Portadown farming family successfully diversify into waste management.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Agriculture Minister Michelle O’Neill, has visited an innovative waste management company to see firsthand how DARD funding has helped the business transform roadside waste into reusable gardening and farming material.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'ReCon Waste Management in Portadown, has received a total of £100,000 in funding through the Rural Development Programme (RDP) to help towards the design, building and installation of a washing and segregation plant and to buy a sludge recovery machine.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Minister O’Neill said: “ReCon is a hugely impressive young company and demonstrates perfectly what can be achieved when a farm business diversifies into new areas to help generate additional income.'
  -
    type: paragraph
    content:
      -
        type: image
        attrs:
          src: 'asset::assets::recon-minister-2015.webp'
          alt: null
      -
        type: text
        text: '“I am delighted that through my department, RDP funding has helped this business to also assist other companies face up to the challenges of environmental legislation and the increasing cost of waste disposal by giving them a viable option.'
  -
    type: paragraph
    content:
      -
        type: text
        text: '“Not only has our funding helped them to develop and expand but also to provide additional employment opportunities for local residents. This is what RDP funding is all about, supporting rural companies and making a real difference to the local communities and creating employment opportunities,” the Minister added.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'ReCon offers a range of recycling services with the main activity being the recycling of gully and sweeper waste. It was finalists in last year’s JFC Innovation Awards.'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'http://www.farminglife.com/news/farming-news/minister-o-neill-visits-innovative-waste-management-project-1-6768364'
              rel: null
              target: null
              title: null
          -
            type: bold
        text: 'First publish on farminglife.com'
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1653496469
image: 2374083641.webp
---
