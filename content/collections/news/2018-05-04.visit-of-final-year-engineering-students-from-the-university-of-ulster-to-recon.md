---
id: 840b9da3-3ad6-4731-86e8-00bd0e08baa4
blueprint: news
title: 'Visit Of Final Year Engineering Students From The University Of Ulster To ReCon'
image: Collage-Re-con-Site-visit-1.webp
text:
  -
    type: set
    attrs:
      values:
        type: video
        video_link: 'https://youtu.be/r3yk386lZeM'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Corvus visit of final year engineering students from the University of Ulster to ReCon Waste Management Ltd.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Yesterday our Health & Safety Recruiter, Michael Smith and our marketing girl, Alex, traded their shoes for wellie boots and a high vis jacket to have a site visit at ReCon Waste Management in Portadown.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Michael had arranged a few students from Ulster University to see what waste management in a private company was all about.'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Check out a full video of the day’s activities:'
  -
    type: paragraph
    content:
      -
        type: text
        text: 'Thank you to Re-Con Waste for taking the time to show us around!'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'https://reconwastemanagement.com/'
              rel: null
              target: null
              title: null
        text: 'https://reconwastemanagement.com/'
  -
    type: paragraph
    content:
      -
        type: text
        marks:
          -
            type: link
            attrs:
              href: 'https://corvus.jobs/'
              rel: null
              target: null
              title: null
        text: 'https://corvus.jobs/'
updated_by: ced564ae-9fc7-4f90-bdac-27e553b64b1c
updated_at: 1651242256
---
