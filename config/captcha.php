<?php

return [
    'service' => 'Recaptcha', // options: Recaptcha / Hcaptcha
    'sitekey' => env('CAPTCHA_SITEKEY', '6Ld6LxsgAAAAAJPm4NY24V9SS5-3IxzBVzP5zcj6'),
    'secret' => env('CAPTCHA_SECRET', '6Ld6LxsgAAAAAKOHfmNa_vunuzDKJDyuJ71KEbNg'),
    'collections' => [],
    'forms' => ['contact','product_enquiry'],
    'user_login' => false,
    'user_registration' => false,
    'disclaimer' => 'Captcha failed.',
    'invisible' => false,
    'hide_badge' => false,
    'enable_api_routes' => false,
];
