# Recon Waste Managment 
DEV: 

username: hello@reflex-studios.com
password: reflexstudios

## Setup
- git clone git@bitbucket.org:reflexstudios/recon.git
- cd /sitename
- composer install
- cp .env.example .env && php artisan key:generate
- If using Tailwind JIT mode make sure package.json has - "watch": "TAILWIND_MODE=watch mix watch",

## Valet:
- valet link
- valet park
- valet open
## Dev:
- npm install
- npm run dev
- npm run watch - If using Tailwind JIT mode make sure package.json has - "watch": "TAILWIND_MODE=watch mix watch",

## Dev Site 
recon.reflex-dev.com
username: reconreflexdev
password: -D7j9Rv7k$QB
ssh -p 4835 reconreflexdev@185.211.22.175
